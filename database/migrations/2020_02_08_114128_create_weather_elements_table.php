<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherElementsTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * Table of meteorological elements - temperature, cloudiness, etc.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_elements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('wind_direction', 50);
            $table->integer('wind_speed');
            $table->double('temperature');
            $table->text('weather')->nullable();
            $table->integer('clouds')->nullable();
            $table->double('pressure');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_elements');
    }
}
