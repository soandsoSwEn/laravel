<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrentWeatherTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * Table of actual weather data
     *
     * @return void
     */
    public function up()
    {
        Schema::create('current_weather', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_station')->unsigned();
            $table->bigInteger('id_weather_elements')->unsigned();
            $table->date('observation_period');
            $table->timestamps();
            $table->index('id_station', 'ixID_station');
            $table->index('id_weather_elements', 'ixID_weatherelements');
            $table->foreign('id_station', 'fkID_currentweather_station')->references('id')->on('stations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_weather_elements', 'fkID_currentweather_weatherelements')->references('id')->on('weather_elements')->onUpdate('cascade')->onDelete('cascade');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropForeign('fkID_currentweather_weatherelements');
        Schema::dropForeign('fkID_currentweather_station');
        Schema::dropIfExists('current_weather');
    }
}
