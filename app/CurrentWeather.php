<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentWeather extends Model
{
    protected $table = 'current_weather';
    
    public function stations()
    {
        return $this->hasOne('App\Station');
    }
    
    public function weather_elements()
    {
        return $this->hasOne('App\WeatherElement');
    }
}
