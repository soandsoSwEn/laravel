<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    protected $table = 'stations';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'icao', 'latitude', 'longitude',
    ];
    
    public function current_weather()
    {
        return $this->hasMany('App\CurrentWeather');
    }
}
