<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeatherElement extends Model
{
    protected $table = 'weather_elements';
    
    public $timestamps = false;
    
    public function current_weather()
    {
        return $this->hasMany('App\CurrentWeather');
    }
}
