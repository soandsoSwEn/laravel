<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\CurrentWeather;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$weathers = CurrentWeather::find(1)->orderBy('created_at', 'desc')->limit(1); //var_dump($weathers); exit;
        $weathers = DB::table('current_weather')
                ->join('stations', 'current_weather.id_station', '=', 'stations.id')
                ->join('weather_elements', 'current_weather.id_weather_elements', '=', 'weather_elements.id')
                ->select('stations.id', 'current_weather.observation_period', 'stations.name', 'weather_elements.temperature', 'weather_elements.pressure')
                ->get();

        return view('welcome')->with('weathers', $weathers);
    }
}
