<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Station;
use App\WeatherElement;
use App\CurrentWeather;
use MetarDecoder\MetarDecoder;
use Exception;

class MetarController extends Controller
{
    private $sourcePath = 'https://aviationweather.gov/metar/data?ids=STATION_NAME&format=raw&date=&hours=0';
    
    /**
     * @var type string International ICAO index
     */
    private $icao;
    
    /**
     * @var type date The period for which the weather report was made
     */
    private $observationPeriod;

    /**
     * @var type int Wind direction
     */
    private $windDirection;
    
    /**
     * @var type int Wind speed
     */
    private $windSpeed;
    
    /**
     * @var type bool Air temperature
     */
    private $airTemperature;
    
    /**
     * @var type int Atmosphere pressure
     */
    private $airPressure;

    /**
     * Receives new weather reports and stores data for each station
     * @return boolean
     */
    public function getData() : bool
    {
        $all_stations = Station::all();
        return $this->getMetar($all_stations);
    }

    /**
     * @param type $all_stations All weather stations
     * @return boolean
     */
    public function getMetar($all_stations) : bool
    {
        foreach ($all_stations as $station) {
            $url = str_replace('STATION_NAME', $station->icao, $this->sourcePath);
            $source = file_get_contents($url);
            $doc = \phpQuery::newDocument($source);
            $metar_report = $doc->find('code')->html();
            $this->decoder($metar_report);
        }
        return true;
    }
    
    /**
     * Decodes data from a weather report
     * @param string $report
     * @return bool
     * @throws Exception
     */
    public function decoder(string $report) : bool
    {
        $decoder = new MetarDecoder();
        $d = $decoder->parse($report);
        
        if($d->isValid()) {
            $sw = $d->getSurfaceWind();
            $this->icao = $d->getIcao();
            $this->observationPeriod = $d->getTime();
            $this->windDirection = $sw->getMeanDirection()->getValue();
            $this->windSpeed = $sw->getMeanSpeed()->getValue();
            $this->airTemperature = $d->getAirTemperature()->getValue();
            $this->airPressure = $d->getPressure()->getValue();
            return $this->putData();
        } else {
            throw new Exception('Metar repor is no corectly!');
        }
    }
    
    /**
     * Saves weather data for a station
     * @return bool
     */
    public function putData() : bool
    {
        /** Id one Station */
        $id = null;
        $station = Station::where('icao', $this->icao)->get();
        foreach ($station as $st) {
            if (strcasecmp($st->icao, $this->icao) == 0) {
                 $id = $st->id;
            }
        }
        
        $weather = new WeatherElement();
        $weather->windDirection = $this->windDirection;
        $weather->windSpeed = $this->windSpeed;
        $weather->temperature = $this->airTemperature;
        $weather->pressure = $this->airPressure;
        $weather->save();
        $id_weather = $weather->id;
        if($id_weather) {
            $current = new CurrentWeather();
            $current->id_station = $id;
            $current->id_weather_elements = $id_weather;
            $current->observationPeriod = date(date("Y-m-d H:i:s"), strtotime($this->observationPeriod));
            return $current->save();
        } else {
            return false;
        }
    }
}
