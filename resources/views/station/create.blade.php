@extends('layouts.app')

@section('content')
<div class="container">
    @auth
    <div class="row justify-content-center mt-5">
        <h2>Adding Station Data</h2>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($errors->any())
            <div class="alert alert-danger">
                <ul class="list list-group">
                    @foreach($errors->all() as $error)
                    <li class="list-group-item text-danger">
                        {{ $error }}
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('station.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Station name</label>
                    <input name="name" type="text" class="form-control" id="exampleStation" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">ICAO Index Number</label>
                    <input name="icao" type="text" class="form-control" id="exampleIcao">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Latitude</label>
                    <input name="latitude" type="number" step="0.1" class="form-control" id="exampleLat">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Longitude</label>
                    <input name="longitude" type="number" step="0.1" class="form-control" id="exampleLong">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    @endauth
</div>
@endsection
