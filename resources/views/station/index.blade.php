@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
        </div>
    </div>

    @auth
    <div class="row justify-content-center mt-5">
        <h2>All Stations</h2>
    </div>
    <div class="row justify-content-center mb-4"><a href="{{ route('station.create') }}" class="btn btn-success pull-right" >New Station</a></div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($stations)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Icao</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($stations as $station)
                    <tr>
                        <th scope="row">{{ $station->id }}</th>
                        <td>{{ $station->name }}</td>
                        <td>{{ $station->icao }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <p>Empty data</p>
            @endif
        </div>
    </div>
    @endauth
</div>
@endsection
